import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

abstract class Account			//Parent class
{
	private int AccNo;
	public int balance;

Account(int a, int b)
{
	AccNo=a;
	balance=b;
}
public void rate(int m,int y)
{
	
}

public void overdraftLimit()
{
	
}

public int get_AccNo()
{
return AccNo;
}

public int get_balance()
{
return balance;
}
public abstract boolean withdraw(double amount);

	
public int Deposit()		//Function to deposit amount
	{
	
	 Scanner s=new Scanner(System.in);

int a;
System.out.println("Enter deposit amount : ");
a=s.nextInt();
try{
	if(a<0)				//Exception when negative amount is deposited
	{
		System.out.println("Can't deposite negative amount.");
		System.out.println("Exception occured while depositting amount.");
	}
}
catch(Exception e){
	System.out.println("Exception handled"+e);
}

balance+=a;

System.out.println("The Balance after deposite " +a + " is " + balance);
s.close();
return balance;
	}

}


class E			//to throw Exception
{  
	public void method() throws Exception{  
	  throw new Exception("Exception occured");  
	 }  
	}  




class Saving extends Account
{


Saving(int a,int b,int c,int d)
{
super(a,b);
}

public void rate(int m,int y)     // calculate interest 

{
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
	String st=dateFormat.format(date);
	String Y = st.substring(0, 4);
	String M = st.substring(5, 7);
	int month = Integer.parseInt(M);
	int year = Integer.parseInt(Y);
	int rateOfInterest = ((month-m)/3 + ((year-y)*12/3))*10;
	int interest =balance*rateOfInterest/100;
	System.out.println("Balance with interest is " + (balance+interest));

}


public boolean withdraw(double amount)
{
double b = amount;

try{
if(b<=balance && (balance-b) >=500)
{
balance-=b;
System.out.println("The Balance After Withdrawing " +b+ " Is " +balance);
}
else				//Exception - when balance goes down to 500
{
	System.out.println("Balance must be minimum 500. Withdraw unsuccessfull.");
	System.out.println("Exception occured");

	E e = new E();
	e.method();

return false;
}
	
}

catch(Exception e){
	System.out.println("Exception handled. "+e);
}
		return true;
}
}

class Current extends Account
{
Scanner s=new Scanner(System.in);

protected int overdraftLimit;

Current(int a,int b,int c)
{
super(a,b);
overdraftLimit=c;
}

public void overdraftLimit() // to set overdraft limit
{
	Scanner s=new Scanner(System.in);
	System.out.println("Enter overdraft limit:");
	overdraftLimit= s.nextInt();
	s.close();
}

public boolean withdraw(double amount)
{

double c = amount;
try{
if(c<=(overdraftLimit+balance))
{
balance-=c;
System.out.println("The Balance After Withdrawing " +c+ " Is " +balance);
}
else				//Exception - when balance goes beyond overdraft limit.
{
	System.out.println("Exception occured during withdrawing from current account");
	System.out.println("Withdraw unsuccessfull. \n /n Balance can't go beyond the overdraft limit.");
   }
}
catch(Exception e)
{
	System.out.println("Exception handled."+e);
}

return true;  
}
}

public class Banking {
	static int A,B,C,D;
	static String N = "";
	static Account a1;
	static int m,y;
	public static void display()			//display menu
	{
		Scanner s=new Scanner(System.in);

		System.out.println("Enter your choice: ");	
 		System.out.println("1. View balance");
	 	System.out.println("2. Deposit Money");
	 	System.out.println("3. Withdraw Money");
	 	System.out.println("4. Exit");
	 	
	 	D=s.nextInt();
	 	
	 	s.close();
	}
	public static void main(String[] args) throws Exception {

		Scanner s=new Scanner(System.in);
		Scanner name = new Scanner(System.in);

		int i;
		
		System.out.println("Press 1 to Create an Account");
		System.out.println("Press 2 to Delete an Account");
		System.out.println("Press 3 to Edit Account Details");
		System.out.println("Press 0 to Exit");
		
		i=s.nextInt();
		switch(i)
		{
		  
		case 1:		//creat account
		int ch;
		System.out.println("Enter your choice :");
		System.out.println("1. Savings Account");
		System.out.println("2. Current Account");
		
		ch=s.nextInt();
		
		System.out.println("Enter Details for your account.\n ");
		
		System.out.println("Name : ");
	 	N="" + name.nextLine();
	 	System.out.println("Account No : ");
	 	A=s.nextInt();
		System.out.println("Opening Balance ");
	 	B=s.nextInt();
	 	try{
	 		if(B<0)
	 		{
	 			E e = new E();
	 			e.method();
	 		}
	 	}
	 	catch(Exception e){
	 		System.out.println("Balance can not be negative!");
	 		
	 	}
		switch(ch)
		{
		case 1:   //creating Savings account
		a1 = new Saving(A,B,C,D);
		
		
		try{
	 	if(B<1000)
	 	{
		 System.out.println("Opening Balance is Low Sorry");
		 System.out.println("Exception occured while opening account.");

	 	E e = new E();			//Exception - when opening balance is low.
	 	e.method();
	 	
	 	
	 	}
	 	else
	 	{
	 		System.out.println("Enter month of creating account: ");
			m=s.nextInt();
			
			System.out.println("Enter year of creating account: ");
			y=s.nextInt();

		 	String filename = "" + A;
		 	
		 	FileOutputStream fout = new FileOutputStream(filename  );		//creat file for savings account
		 	ObjectOutputStream oout = new ObjectOutputStream(fout);
		 	 String S= "Account no :"+ A +"\n\n Balance : "+B +"\n\n Name : "+N ;
		 	oout.writeUTF(S);
		   
		    oout.close();
		    fout.close();
		 	System.out.println("The Savings Account Is SuccesFully Created");

	 	}
		}
		catch(Exception e)
		{
			System.out.println("Exception handled."+e);
			break;
		}
		
		
	 	break;
	 	
	 	
		case 2:    // creating Current account
		a1 = new Current(A,B,C);
		String filenamec = "" + A;

	 	FileOutputStream fcout = new FileOutputStream(filenamec);		//creat file for current account
	 	ObjectOutputStream ocout = new ObjectOutputStream(fcout);
	 	 String Sc= "Account no :"+ A +"\n\n Balance : "+B ;
	 	ocout.writeUTF(Sc);
	   
	    ocout.close();
		
		
		
		a1.overdraftLimit();
		System.out.println("The Current Account Is SuccesFully Created");
		break;
		}
		
	 	do{
	 	display();
	 	
	 	switch(D)
	 	{
	 	case 1:   //check balance
	 	
	 	System.out.println("Your details are :  ");
	 	System.out.println("Account No. : "+a1.get_AccNo());
	 	System.out.println("Account balance : "+a1.get_balance());
	 	break;
	 	
	 	case 2:   //Deposit
	 	
	 	a1.Deposit();
	 	if(ch==1)
	 	{
	 	System.out.println("Rate of interest per 3 months is 10%");
	 	a1.rate(m, y);
	 	}
	 	break;
	 	case 3:   //Withdraw
	 		int b;
	 		
	 		System.out.println("Enter withdraw amount : ");
	 		b=s.nextInt();

	 	a1.withdraw(b);
	 	break;
	 	case 4: // exit
	 		return;
	 	}
	 	
	 	}while(A!=0);
	 	break;
		case 2: //Delete account
			Scanner S = new Scanner(System.in);
			System.out.println("enter account no.:");
			int accNo = S.nextInt();
			String filenamed = "" + accNo;
    		File file = new File(filenamed);
    		try{
    		if(file.exists())
    		{
    		file.delete();
    		System.out.println("Account " + accNo + " deleted.");
    		}
    		else
    		{
    			System.out.println("Account does not exist!");
    			System.out.println("Failed to delete account");
    			E e = new E();			//Exception while deleting account
    			e.method();
    		}
    		}
    		
    		catch(Exception e){
    			System.out.println("Exception handled"+e);
    		}
    		break;
		case 3:
			System.out.println("Enter new details ");
			System.out.println("Enter Name:");
			N=name.nextLine();
			System.out.println("Enter Account No.");
			A=s.nextInt();
			System.out.println("Account details successfully updated.");
			
			String filenamec = "" + A;

		 	FileOutputStream fcout = new FileOutputStream(filenamec);		//creat file for current account
		 	ObjectOutputStream ocout = new ObjectOutputStream(fcout);
		 	 String Sc= "Account no :"+ A +"\n\n Balance : "+B ;
		 	ocout.writeBytes(Sc);
		   
		    ocout.close();
			
			
		case 0:
			return;
		

	}	
		s.close();
		name.close();
	}
}


