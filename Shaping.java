import java.util.*;
import java.io.*;

class Shape			//Parent class
{
	public String colour;
	
	public void set_colour(String c)
	{
		colour=c;
	}
	public String get_colour()
	{
		return colour;
	}
}

class TwoDim extends Shape		//child class of shape class
{
	public void perimeter(int a,int b)
	{
		float perimeter;
		perimeter=2*(a+b);
		System.out.println("Perimeter of rectangle is:"+perimeter);
		
	}
	
	public void peri_cir(int r)		//function to calculate perimeter of circle
	{
		
		double cir;
		cir=2*3.14*r;
		System.out.println("\n Circumference of circle is:"+cir);
	}
	public void area(int a,int b)		//function to calculate area of rectangle
	{
		float Area;
		Area=a*b;
		System.out.println("\nArea of rectangle is:"+Area);
	}
	public void area_cir(int r)		//function to calculate area of circle
	{
		double Area;
		Area=3.14*r*r;
		System.out.println("\nArea of circle is:"+Area);
	}
	
}

class Rectangle extends TwoDim
{
	Scanner re=new Scanner(System.in);
	TwoDim T=new TwoDim();

	Rectangle()
	{
		float A=0,B=0;                                         //vertices of rectangle
	}
	

}
class Circle extends TwoDim
{
	
	Circle()
	{
		float center=0,r=0,D;

	}
	
	
}

class Threedimen extends Shape
{
	public void surfacearea_cub(int a,int b,int c)			//function to calculate 
	{
		
		float SA;
		SA=2*(a*b+b*c+c*a);
		System.out.println("Surface area of cuboid is:"+SA);
	}
	
	public void volume_cub(int a,int b,int c)			//function to calculate volume of cuboid
	{
		float V;
		V=a*b*c;
		System.out.println("Volume of cuboid is:"+V);
	}
	
	public void area_sphere(int r)			//function to calculate area of sphere
	{
		
		double S;
		S=4*3.14*r*r;
		System.out.println("\nSurface area of Sphere is:"+S);
	}
	
	public void volume_sphere(int r)		//function to calculate volume of sphere
	{
		
		double Vol;
		Vol=4/3*3.14*r*r*r;
		System.out.println("\n Volume of Sphere is:"+Vol);
	}
}

class cuboid extends Threedimen
{

	cuboid()
	{
	float a,b,c;
	}
	
}

class Sphere extends Threedimen
{
	Sphere()
	{
	float Center=0,r,D;
	}
	
	
	
	
}

public class Shaping 
{
	public static void main(String arg[]) throws Exception
	{
		int choice;
		Shape Sh=new Shape();
		System.out.println("\n Enter colour:"+Sh.get_colour());
		Scanner l=new Scanner(System.in);
		Scanner s=new Scanner(System.in);

		
		System.out.println("\n Enter your choice,which type of object do you want:");
		System.out.println("\n1.TwoDim");
		System.out.println("\n2.Threedimen");
		choice=l.nextInt();
		
		switch(choice)
		{
		case 1: 			//2-D
			int ch;
			Sh=new TwoDim();
			System.out.println("\nWhich object you want to create:");
			System.out.println("\n1.Rectangle");
			System.out.println("\n2.Circle");
			ch = s.nextInt();
			switch(ch)
			{
			case 1:                                      //Rectangle
				TwoDim Two=new TwoDim();
				Two=new Rectangle();
				new File("D:\\Rectangle").mkdir();
				FileOutputStream fout=new FileOutputStream("Rectangle.dat");
				ObjectOutputStream Out=new ObjectOutputStream(fout);
				Rectangle R1=new Rectangle();
				
				System.out.println("Enter vertices of rectangle:\nEnter a:\nEnter b:");
				
				int a=s.nextInt();
				int b=s.nextInt();
				Two.perimeter(a,b);
				Two.area(a,b);
				break;
				
			case 2:                                       //Circle
				Two=new Circle();
				new File("D:\\Circle").mkdir();
				FileOutputStream Fout=new FileOutputStream("Circle.dat");
				ObjectOutputStream out=new ObjectOutputStream(Fout);
				Circle C1=new Circle();
				int center=0;
				System.out.println("\n center of circle is:"+center);
				
				System.out.println("\n Enter diameter of the Circle:");
				int D=s.nextInt();
				
				System.out.println("Enter radius of the Circle:");
				int r=s.nextInt();
				
				C1.peri_cir(r);
				C1.area_cir(r);
				
				break;
			}
			break;
		case 2:                                                //3-D
			int choic;
			Sh=new Threedimen();
			System.out.println("\nWhich object you want to create:");
			System.out.println("\n1.Cuboid");
			System.out.println("\n2.Sphere");
			choic=s.nextInt();

			switch(choic)
			{
			case 1:                                            //cuboid
				Threedimen Three=new Threedimen();
				Three=new cuboid();
				new File("D:\\Cuboid").mkdir();
				FileOutputStream Fout=new FileOutputStream("Cuboid.dat");
				ObjectOutputStream out=new ObjectOutputStream(Fout);
				cuboid C=new cuboid();
				
				System.out.println("\n Enter the vertices of cuboid:\n Enter a:\nEnter b:\nEnter c:");
				int a=s.nextInt();
				int b=s.nextInt();
				int c=s.nextInt();
				
				C.surfacearea_cub(a,b,c);
				C.volume_cub(a,b,c);
				
				
				break;
				
			case 2:                                            //Sphere
				Three=new Sphere();
				try
				{
				new File("D:\\Sphere").mkdir();
				FileOutputStream fout=new FileOutputStream("Sphere.dat");
				ObjectOutputStream Out=new ObjectOutputStream(fout);
			}
				catch(Exception e)
				{
					System.out.println("Exception occured"+e);
				}
				Sphere S=new Sphere();
				int Center=0,D=0,r=0;
				System.out.println("\n center of Sphere is:"+Center);
				Scanner ss = new Scanner(System.in);
				System.out.println("\n Enter diameter of the Sphere:");
				D=ss.nextInt();
				
				System.out.println("Enter radius of the Sphere:");
				r=ss.nextInt();
				
				S.area_sphere(r);
				S.volume_sphere(r);
				
				break;
			}
			break;
		}
	}
}

