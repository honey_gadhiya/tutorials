#include <stdio.h>
#include <conio.h>
#include<iostream>
using namespace std;
void push();
void pop();
void display();

class node
{
public:
 int info;
 class node *link;
} *top = NULL;

int item;

main()
{
 int ch;

 do
 {
 cout<<"\n\n1. Push\n2. Pop\n3. Display\n4. Exit\n"<<endl;
 cout<<"\nEnter your choice: "<<endl;
 cin>>ch;

 switch(ch)
 {
 case 1:
 push();
break;

 case 2:
 pop();
break;

 case 3:
 display();
break;

 case 4:
 break;

 default:
 cout<<"Invalid choice. Please try again.\n"<<endl;
 }
 } while(1);
 return 0;
}

void push()
{
 struct node *ptr;

 cout<<"\n\nEnter ITEM: "<<endl;
 cin>>item;

 if (top == NULL)
 {
 top = new node;
 top->info = item;
 top->link = NULL;
 }
 else
 {
 ptr = top;
 top = new node;
 top->info = item;
 top->link = ptr;
 }

 cout<<"\nItem inserted: %d\n"<<item<<endl;
}

void pop()
{
 class node *ptr;

 if (top == NULL)
 cout<<"\n\nStack is empty\n"<<endl;
 else
 {
 ptr = top;
 item = top->info;
 top = top->link;


 cout<<"\n\nItem deleted: %d"<<item<<endl;
 }
}

void display()
{
 struct node *ptr;

 if (top == NULL)
 cout<<"\n\nStack is empty\n"<<endl;
 else
 {
 ptr = top;

 while(ptr != NULL)
 {
 cout<<"\n\n%d"<<ptr->info<<endl;
 ptr = ptr->link;
 }
 }
}
